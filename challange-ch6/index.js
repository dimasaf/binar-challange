const express = require("express");
const app = express();
const path = require("path");

const { user_game } = require("./models");
const { user_game_history } = require("./models");
const { user_biodata } = require("./models");

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/main.js"));
app.use(express.json());

app.get("/", (req, res) => {
  res.render(path.join(__dirname, "views/login.ejs"), { url: "/login" });
});

app.post("/login", async (req, res) => {
  const { userName, passWord } = req.body;
  const users = await user_game.findAll({
    where: {
      userName: userName,
      passWord: passWord,
    },
  });

  if (users.length) {
    return res.redirect(301, `/dashboard/`);
  } else if (users.length === 0 || users === 0) {
    return res.status(401).json("unauthorized");
  } else {
    return res.status(400).end();
  }
});

app.get("/dashboard", (req, res) => {
  const { username } = req.params;
  user_game_history
    .findAll()
    .then((history) => {
      res.render(path.join(__dirname, "views/dashboard.ejs"), {
        history,
      });
    })
    .catch((err) => {
      res.status(400).json(err);
    });
});

app.get("/profile", (req, res) => {
  user_biodata
    .findAll()
    .then((biodata) => {
      res.render(path.join(__dirname, "views/profile.ejs"), {
        biodata,
      });
    })
    .catch((err) => {
      res.status(400).json(err);
    });
});

app.post("/add", (req, res) => {
  user_game_history
    .create({
      name: req.body.name,
      gameStatus: req.body.flexRadioDefault === "true" ? true : false,
    })
    .then(() => {
      res.redirect(301, `/dashboard/`);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
});

app.post("/delete/:id", (req, res) => {
  user_game_history
    .destroy({
      where: { id: req.params.id },
    })
    .then(() => {
      res.redirect(301, `/dashboard/`);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
});

app.post("/update/:id", (req, res) => {
  console.log(req.params);
  user_game_history
    .update(
      {
        name: req.body.title,
        gameStatus: req.body.flexRadioDefault === "true" ? true : false,
      },
      {
        where: { id: req.params.id },
      }
    )
    .then(() => {
      res.redirect(301, `/dashboard/`);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
});

app.listen(3000, () => {
  console.log("server running");
});
