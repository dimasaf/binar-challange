import { createContext, useContext, useState } from "react";

export const MenuContext = createContext();
export const useMenuActive = () => useContext(MenuContext);

export const MenuProvider = ({ children }) => {
  const [active, setActive] = useState(1);

  const setMenuHome = () => {
    setActive(1);
  };
  const setMenuCreate = () => {
    setActive(2);
  };

  return (
    <MenuContext.Provider value={{ active, setMenuCreate, setMenuHome }}>
      {children}
    </MenuContext.Provider>
  );
};

export default MenuProvider;
