import { createContext, useContext, useState } from "react";

export const UserContext = createContext();
export const useCreateUser = () => useContext(UserContext);

export const CreateUserProvider = ({ children }) => {
  const [dataUser, setDataUser] = useState([]);

  return (
    <UserContext.Provider value={{ setDataUser, dataUser }}>
      {children}
    </UserContext.Provider>
  );
};

export default CreateUserProvider;
