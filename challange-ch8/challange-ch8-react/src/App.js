import Sidebar from "./Components/Sidebar";
// import { useState } from "react";
import MainLayout from "./Components/MainLayout";
import { MenuProvider } from "./Context/menu";
import { CreateUserProvider } from "./Context/user";

function App() {
  return (
    <MenuProvider>
      <CreateUserProvider>
        <div className="flex min-w-min">
          <Sidebar />
          <div className="p-8 container overflow-auto max-w-full bg-slate-100">
            <MainLayout />
          </div>
        </div>
      </CreateUserProvider>
    </MenuProvider>
  );
}

export default App;
