import React from "react";
import MenuList from "../MenuList";
import MenuCreate from "../MenuCreate";
import { useMenuActive } from "../../Context/menu";

const MainLayout = () => {
  const menuActive = useMenuActive();
  const { active } = menuActive;

  return (
    <div>
      {active === 1 ? <MenuList /> : null}
      {active === 2 ? <MenuCreate /> : null}
    </div>
  );
};

export default MainLayout;
