import { useState } from "react";
import { BsArrowRightCircleFill, BsSearch } from "react-icons/bs";
import { useMenuActive } from "../../Context/menu";

const SideMenu = [
  {
    id: 1,
    value: "Home",
  },
  {
    id: 2,
    value: "Add Player",
  },
];

function Sidebar() {
  const menuActive = useMenuActive();
  const { setMenuCreate, setMenuHome } = menuActive;
  const [open, setOpen] = useState(true);
  const [activeItem, setActiveItem] = useState(1);

  const handleMenu = (id) => {
    if (id === 1) {
      setMenuHome();
      setActiveItem(id);
    } else if (id === 2) {
      setMenuCreate();
      setActiveItem(id);
    }
  };

  return (
    <div
      className={`${
        open ? "w-72" : "w-20"
      } duration-300 h-screen bg-gray-800 p-6 text-slate-300`}
    >
      <div className="relative">
        <BsArrowRightCircleFill
          className={`absolute bg-white text-gray-700 -right-10 top-16 rounded-full text-4xl cursor-pointer border-slate-900 ${
            open && "rotate-180"
          }`}
          onClick={() => setOpen(!open)}
        />
      </div>
      <div
        className="flex items-center rounded-md bg-gray-600 py-2 px-3 mt-2 mb-16 cursor-pointer"
        onClick={() => setOpen(true)}
      >
        <BsSearch className={`${open && "mr-2"}  duration-300`} />
        <input
          placeholder="Search.."
          className={`bg-transparent block float-left text-base focus:outline-none duration-300 ${
            !open && "hidden"
          }
          `}
        />
      </div>

      {SideMenu.map((item) => (
        <div
          className={`text-2xl duration-100 cursor-pointer ${
            !open && "scale-0"
          } ${
            item.id === activeItem ? "bg-slate-900" : ""
          } -ml-6 -mr-6 p-2 mb-2`}
          onClick={() => handleMenu(item.id)}
        >
          {item.value}
        </div>
      ))}
    </div>
  );
}

export default Sidebar;
