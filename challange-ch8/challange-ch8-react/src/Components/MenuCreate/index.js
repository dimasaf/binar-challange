import { useState } from "react";
import { useCreateUser } from "../../Context/user";
import Alert from "../Alert";

function MenuCreate() {
  const createUser = useCreateUser();
  const { dataUser, setDataUser } = createUser;
  const [user, setUser] = useState({
    id: "",
    email: "",
    username: "",
    experience: "",
  });
  const [successAlert, setSuccessAlert] = useState(false);

  const handleSubmit = (e) => {
    setDataUser([...dataUser, user]);
    setUser({
      id: "",
      email: "",
      username: "",
      experience: "",
    });
    setSuccessAlert(true);
  };

  const handleClose = () => {
    setSuccessAlert(false);
  };

  return (
    <div>
      {successAlert && <Alert onClick={handleClose} />}

      <div className="bg-white px-6 shadow-xl ring-1 ring-gray-900/5 sm:mx-auto sm:max-w-lg sm:rounded-lg sm:px-10 mt-40">
        <h1 className="text-xl py-5 text-center ">Create Player</h1>
        <div className="flex-row ">
          <input
            placeholder="Username"
            type="text"
            className="bg-slate-50 float-left text-base focus:outline-none duration-300 w-full items-center rounded-md  py-2 px-3 my-2  cursor-pointer  border border-slate-200"
            onChange={(e) =>
              setUser({
                ...user,
                username: e.target.value,
                id: Math.floor(Math.random() * (999 - 100 + 1) + 100),
              })
            }
            value={user.username}
          />
          <input
            placeholder="Email"
            type="email"
            name="email"
            className="bg-slate-50 float-left text-base focus:outline-none duration-300 w-full items-center rounded-md  py-2 px-3 my-2  cursor-pointer  border border-slate-200"
            onChange={(e) => setUser({ ...user, email: e.target.value })}
            value={user.email}
          />
          <input
            placeholder="Experience"
            className="bg-slate-50 float-left text-base focus:outline-none duration-300 w-full items-center rounded-md  py-2 px-3 my-2  cursor-pointer  border border-slate-200"
            onChange={(e) => setUser({ ...user, experience: e.target.value })}
            value={user.experience}
          />

          <button
            className="p-4 text-center rounded-lg w-full bg-cyan-800 my-4 text-white font-bold uppercase hover:bg-cyan-700 duration-200"
            onClick={(e) => handleSubmit()}
          >
            submit
          </button>
        </div>
      </div>
    </div>
  );
}

export default MenuCreate;
