import { BsTrashFill } from "react-icons/bs";
import { useCreateUser } from "../../Context/user";

function MenuList() {
  const createUser = useCreateUser();
  const { dataUser, setDataUser } = createUser;

  const handleDelete = (id) => {
    setDataUser(dataUser.filter((el) => el.id !== id));
  };

  return (
    <div>
      <h1 className="text-xl mb-8">Player List</h1>
      <div className="overflow-auto rounded-lg shadow md:block">
        <table className="w-full">
          <thead className="bg-gray-50 border-b-2 border-gray-200">
            <tr>
              <th className="w-20 p-3 text-sm font-semibold tracking-wide text-left">
                No.
              </th>
              <th className="p-3 text-sm font-semibold tracking-wide text-left">
                Email
              </th>
              <th className=" w-60 p-3 text-sm font-semibold tracking-wide text-left">
                Username
              </th>
              <th className="w-36 p-3 text-sm font-semibold tracking-wide text-left">
                Experience
              </th>
              <th className="p-3 text-sm font-semibold tracking-wide text-left">
                Level
              </th>
              <th className="p-3 text-sm font-semibold tracking-wide text-center">
                Action
              </th>
            </tr>
          </thead>

          {console.warn(dataUser)}
          <tbody className="divide-y divide-gray-100">
            {dataUser.map((item) => {
              return (
                <tr className="bg-white ">
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap">
                    <a
                      href="#"
                      className="font-bold text-blue-500 hover:underline"
                    >
                      {item.id}
                    </a>
                  </td>
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap">
                    {item.email}
                  </td>
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap">
                    <span className="p-1.5 text-xs font-medium uppercase tracking-wider text-green-800 bg-green-200 rounded-lg bg-opacity-50">
                      {item.username}
                    </span>
                  </td>
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap">
                    {item.experience || 100}
                  </td>
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap">
                    {item.level || 1}
                  </td>
                  <td className="p-3 py-5 text-sm text-gray-700 whitespace-nowrap flex justify-around">
                    <BsTrashFill onClick={() => handleDelete(item.id)} />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default MenuList;
