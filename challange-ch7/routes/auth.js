var express = require("express");
var router = express.Router();

// Controllers
const auth = require("../controllers/authController");

// Homepage
router.get("/", (req, res) => res.render("index"));

// Register Page
router.get("/register", (req, res) => res.render("register"));
router.post("/register", auth.register);

// Login
router.get("/login", (req, res) => res.render("login"));
router.post("/login", auth.login);

// Middleware
const restrict = require("../middlewares/restrict");
router.get("/", restrict, (req, res) => res.render("index"));

router.get("/dashboard", restrict, auth.whoami);

module.exports = router;
