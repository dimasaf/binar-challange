const express = require("express");
const app = express();

const users = [];

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/main.js"));

app.get("/register", (req, res) => {
  res.render("register");
});

app.post("/register", (req, res) => {
  const { email, password } = req.body;

  users.push({ email, password });
  res.redirect("/");
});

app.use("/game", (req, res) => {
  res.render("game");
});

app.use("/", (req, res) => {
  res.render("index");
});

app.listen(3000);
